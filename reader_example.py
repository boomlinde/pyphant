import phant

db = phant.reader(
    'https://data.sparkfun.com/output/aGRVr0MQ4NHbwxWaZRLv',
    'test',
    value=int,
    key=str
)

for row in db.execute('SELECT * FROM test ORDER BY datetime(timestamp) ASC'):
    print row
